﻿
Public Class WindowWrapper
    Implements System.Windows.Forms.IWin32Window
    'Private ReadOnly _hwnd As IntPtr
    Dim OutlookProcess() As System.Diagnostics.Process
    Dim _hwnd As System.IntPtr
    Public Sub New()
        Try
            OutlookProcess = System.Diagnostics.Process.GetProcessesByName("OUTLOOK")
            If OutlookProcess.Length = 0 Then
                log("Running outlook process id returned 0")

            ElseIf OutlookProcess.Length = 1 Then
                log("Outlook ProcessId " & OutlookProcess(0).MainWindowHandle.ToString)
                _hwnd = OutlookProcess(0).MainWindowHandle
            Else
                For i = 0 To OutlookProcess.Length - 1
                    If CInt(OutlookProcess(i).MainWindowHandle.ToString) <> 0 Then
                        _hwnd = OutlookProcess(i).MainWindowHandle
                        log("Outlook ProcessId index " & i & ": of # " & OutlookProcess.Length & ", id:" & OutlookProcess(i).MainWindowHandle.ToString)
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            log("Exception determining outlook id:" & ex.Message)
        End Try
        
    End Sub

    Public ReadOnly Property Handle As System.IntPtr Implements System.Windows.Forms.IWin32Window.Handle
        Get
            Return _hwnd
        End Get
    End Property
End Class
