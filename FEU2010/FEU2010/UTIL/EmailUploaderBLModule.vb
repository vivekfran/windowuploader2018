﻿Imports Microsoft.Office.Interop.Outlook
Imports System.IO
Imports FEU2010.Util



Module EmailUploaderBLModule


    Public Const M_SCRIPT = "jsp/outlookMail.jsp"
    Public Const TEMPFOLDERPATH As String = "\FranConnect\EmailUploaderAddIN\Temp\"
    Public TEMP_MAIL_FOLDER As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString & TEMPFOLDERPATH
    Public OIDRefs As Collection
    Public currentUploadStatusCollection As Collections.Specialized.StringDictionary
    Public CredentialsValidationSuccess As Boolean = False


    Public Function GetOutlookItems() As Collection

        Dim m_SelectedMails As New Collection

        Dim olApp As New Outlook.Application
        Dim olExp As Outlook.Explorer
        Dim olSel As Outlook.Selection
        Dim OlMailItem As Outlook.MailItem
        Dim nCtr As Integer





        Dim xmlFileName As String
        Dim htmlFileName As String
        Dim AttachmentFileName As String

        Dim mailBody As String
        Dim mailFormat As Integer
        Dim currentMailFiles() As String 'index 0:for xml file, 1: for document/html, and subsequent for attachments
        Dim NewFileId As String

        olExp = olApp.ActiveExplorer  ' Get the ActiveExplorer.
        olSel = olExp.Selection       ' Get the selection.

        'If any outlook folder is opened
        olExp = olApp.ActiveExplorer
        If olExp Is Nothing Then
            Return m_SelectedMails      'return blank collection
        End If

        'If Mails are Selected
        olSel = olExp.Selection

        If olSel IsNot Nothing And olSel.Count > 0 Then
            verifyFolderPath(TEMP_MAIL_FOLDER)
            OIDRefs = New Collection
        Else
            Return m_SelectedMails      'return blank collection
        End If

        Dim colPos As Integer = 0
        'Iterate all the selected items to save in the temp folder
        For nCtr = 1 To olSel.Count


           
            'Check if other than mail items are selected
            If TypeName(olSel.Item(nCtr)) = "MailItem" Then

                OlMailItem = CType(olSel.Item(nCtr), MailItem)

                mailBody = ""

                NewFileId = Format(Now, "yyyyMMddhhmmss" & nCtr & Right(Format(Timer, "#0.000"), 3))
                If NewFileId = "" Then
                    NewFileId = OlMailItem.EntryID
                    log("Error: NewFileId can't be generated, Using Outlook entryid as NewfileID: " & NewFileId)
                End If

                ReDim currentMailFiles(1)
                xmlFileName = TEMP_MAIL_FOLDER & NewFileId & ".xml"
                makeFile(GenerateContentDetailXML(OlMailItem), xmlFileName)
                currentMailFiles(0) = xmlFileName
                log("XmlFile created:===============" & xmlFileName)

                mailFormat = OlMailItem.BodyFormat

                'fetch mail body
                Select Case mailFormat
                    Case 1
                        mailBody = OlMailItem.Body
                        htmlFileName = TEMP_MAIL_FOLDER & NewFileId & ".txt"
                        currentMailFiles(1) = htmlFileName

                    Case 2, 3
                        mailBody = "To...   " & OlMailItem.To & "<BR>" & "Cc...   " & OlMailItem.CC & "<BR>"
                        mailBody = mailBody & "Subject:   " & OlMailItem.Subject & "<BR><BR>"
                        'If Not String.IsNullOrEmpty(Util.pids) Then
                        '    mailBody = mailBody & "primaryids:   " & Util.pids & "<BR><BR>"
                        'End If

                        mailBody = mailBody & Replace(OlMailItem.HTMLBody, "&#8211;", "")
                        htmlFileName = TEMP_MAIL_FOLDER & NewFileId & ".html"
                        currentMailFiles(1) = htmlFileName
                    Case Else
                        OlMailItem.BodyFormat = OlBodyFormat.olFormatHTML
                        mailBody = "To...   " & OlMailItem.To & "<BR>" & "Cc...   " & OlMailItem.CC & "<BR>"
                        mailBody = mailBody & "Subject:   " & OlMailItem.Subject & "<BR><BR>"
                        'If Not String.IsNullOrEmpty(Util.pids) Then
                        '    mailBody = mailBody & "primaryids:   " & Util.pids & "<BR><BR>"
                        'End If
                        mailBody = mailBody & Replace(OlMailItem.HTMLBody, "&#8211;", "")
                        htmlFileName = TEMP_MAIL_FOLDER & NewFileId & ".html"
                        currentMailFiles(1) = htmlFileName
                End Select

                'Save Attachments
                ReDim Preserve currentMailFiles(OlMailItem.Attachments.Count + 1)

                Dim attachments = OlMailItem.Attachments
                For Each attachment As Outlook.Attachment In attachments
                    Dim EmbededAttachmentId As String
                    Const EMBEDDED_ATTACHMENT_SCHEMA = "http://schemas.microsoft.com/mapi/proptag/0x3712001E"
                    EmbededAttachmentId = CStr(attachment.PropertyAccessor.GetProperty(EMBEDDED_ATTACHMENT_SCHEMA))

                    'Handle Embeded attachments separately here
                    If (EmbededAttachmentId = "") Then
                        'AttachmentFileName = TEMP_MAIL_FOLDER & NewFileId & "_" & attachment.Index() & "_" & Replace(attachment.FileName, "_", "-")
                        AttachmentFileName = TEMP_MAIL_FOLDER & NewFileId & attachment.Index() & "_" & Replace(attachment.FileName, "_", "-")
                        log("General Attachment: " & TEMP_MAIL_FOLDER & NewFileId & attachment.Index() & "_" & Replace(attachment.FileName, "_", "-"))
                    Else
                        log("Embeded Attachment: " & TEMP_MAIL_FOLDER & NewFileId & attachment.Index() & "_" & Replace(attachment.FileName, "_", "-"))
                        AttachmentFileName = TEMP_MAIL_FOLDER & NewFileId & attachment.Index() & "_" & Replace(attachment.FileName, "_", "-")
                        EmbededAttachmentId = "cid:" & EmbededAttachmentId
                        '//below teo lines are 
                        'mailBody = Replace(mailBody, EmbededAttachmentId, "999prembed999" & Replace(attachment.FileName, "_", "-"))
                        'AttachmentFileName = TEMP_MAIL_FOLDER & NewFileId & attachment.Index() & "_" & "999prembed999" & Replace(attachment.FileName, "_", "-")
                    End If

                    currentMailFiles(attachment.Index + 1) = AttachmentFileName
                    attachment.SaveAsFile(AttachmentFileName)
                Next attachment

                'Create body html
                makeFile(mailBody, htmlFileName)
                log("htmlFile created:===============" & htmlFileName)

                'Add the mail details to the collection
                m_SelectedMails.Add(currentMailFiles, NewFileId, , colPos)
                log(OlMailItem.EntryID & "---" & NewFileId)
                OIDRefs.Add(OlMailItem.EntryID, NewFileId, , colPos)
                colPos = colPos + 1
            Else
                log("Skipped...because this selected outlook item is not a mail item. ")
            End If

        Next nCtr
        log("Mail collection completed, calling upload..")

        Return m_SelectedMails
        'collection has been created; call upload method

        'UploadHandler(m_SelectedMails)



    End Function

    Private Function GenerateContentDetailXML(ByVal OlMailItem As Outlook.MailItem) As String
        'Holds the Header Information
        Dim strHeaderInfo As String
        strHeaderInfo = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf

        strHeaderInfo = strHeaderInfo & "<MailItem>" & vbCrLf

        On Error Resume Next

        '// start registered user info tag
        strHeaderInfo = strHeaderInfo & "<UserUploadToken>" & getProp("uploadtoken") & "</UserUploadToken>" & vbCrLf
        '/

        'Set Importance Icon
        Select Case OlMailItem.Importance
            Case OlImportance.olImportanceHigh
                strHeaderInfo = strHeaderInfo & "<Importance>High</Importance>" & vbCrLf
            Case OlImportance.olImportanceLow
                strHeaderInfo = strHeaderInfo & "<Importance>Low</Importance>" & vbCrLf
            Case OlImportance.olImportanceNormal
                strHeaderInfo = strHeaderInfo & "<Importance>Normal</Importance>" & vbCrLf
        End Select


        ' Set Flag Icon
        Select Case OlMailItem.FlagStatus
            Case OlFlagStatus.olFlagComplete
                strHeaderInfo = strHeaderInfo & "<FlagStatus>FlagComplete</FlagStatus>" & vbCrLf
            Case OlFlagStatus.olFlagMarked
                strHeaderInfo = strHeaderInfo & "<FlagStatus>FlagMarked</FlagStatus>" & vbCrLf
            Case OlFlagStatus.olNoFlag
                strHeaderInfo = strHeaderInfo & "<FlagStatus>NoFlag</FlagStatus>" & vbCrLf
        End Select

        'Set Attachment Icon
        strHeaderInfo = strHeaderInfo & "<Attachments>" & OlMailItem.Attachments.Count & "</Attachments>" & vbCrLf

        'Set unread Icon
        strHeaderInfo = strHeaderInfo & "<UnRead>" & OlMailItem.UnRead & "</UnRead>" & vbCrLf

        'Set SenderName
        strHeaderInfo = strHeaderInfo & "<SenderName>" & ReplaceXMLSpecChar(OlMailItem.SenderName) & "</SenderName>" & vbCrLf

        'Set Received time
        strHeaderInfo = strHeaderInfo & "<ReceivedTime>" & Format(OlMailItem.ReceivedTime, "yyyy-MM-dd HH:mm:ss") & "</ReceivedTime>" & vbCrLf

        ''mailBody = mailBody & "tabPrimaryId:   " & "291290720" & "<BR><BR>" <primaryids>sales_fs_830070808</primaryids> 


        'set PrimaryID

        If Not String.IsNullOrEmpty(Util.pids) Then
            strHeaderInfo = strHeaderInfo & "<primaryids>" & Util.pids & "</primaryids>" & vbCrLf
        End If

        'Set Subject 
        strHeaderInfo = strHeaderInfo & "<Subject>" & ReplaceXMLSpecChar(OlMailItem.Subject) & "</Subject>" & vbCrLf

        'Set Body 
        strHeaderInfo = strHeaderInfo & "<Body>" & ReplaceXMLSpecChar(OlMailItem.Body) & "</Body>" & vbCrLf

        'Set SenderEmailAddress Column
        strHeaderInfo = strHeaderInfo & "<From>" & ReplaceXMLSpecChar(OlMailItem.SenderEmailAddress) & "</From>" & vbCrLf

        'Set mail To
        Dim MailTo As String = ResolveMailRecipients(OlMailItem, OlMailRecipientType.olTo)

        strHeaderInfo = strHeaderInfo & "<To>" & ReplaceXMLSpecChar(MailTo) & "</To>" & vbCrLf

        'Set mail CC
        Dim MailCc As String = ResolveMailRecipients(OlMailItem, OlMailRecipientType.olCC)
        strHeaderInfo = strHeaderInfo & "<CC>" & ReplaceXMLSpecChar(MailCc) & "</CC>" & vbCrLf

        'Set Sent Time Column
        strHeaderInfo = strHeaderInfo & "<SentOn>" & Format(OlMailItem.SentOn, "yyyy-MM-dd HH:mm:ss") & "</SentOn>" & vbCrLf

        'Set Size Column
        strHeaderInfo = strHeaderInfo & "<Size>" & Format(OlMailItem.Size / 1024, "###,###,##0") & " KB" & "</Size>" & vbCrLf

        'Close root xml tag
        strHeaderInfo = strHeaderInfo & "</MailItem>"

        'Return the XML now
        GenerateContentDetailXML = strHeaderInfo

    End Function

    Private Function ResolveMailRecipients(ByVal mail As Outlook.MailItem, ByVal recipientType As Outlook.OlMailRecipientType) As String
        Dim Result As String = ""
        Dim i As Integer
        Dim recipients As Outlook.Recipients = mail.Recipients

        If recipients IsNot Nothing Then
            Try
                For i = 1 To recipients.Count
                    Dim recipient As Outlook.Recipient = recipients.Item(i)
                    If recipient IsNot Nothing Then
                        Try
                            If recipient.Type = CInt(recipientType) Then
                                Dim entry As Outlook.AddressEntry = recipient.AddressEntry

                                If entry IsNot Nothing Then
                                    Try
                                        If entry.Address.IndexOf("@") < 0 Then
                                            Dim exchangeuser As Outlook.ExchangeUser = recipient.AddressEntry.GetExchangeUser
                                            If Result = "" Then
                                                Result = exchangeuser.PrimarySmtpAddress
                                            Else
                                                Result += "," & exchangeuser.PrimarySmtpAddress
                                            End If

                                        Else
                                            If Result = "" Then
                                                Result = entry.Address
                                            Else
                                                Result += "," & entry.Address
                                            End If

                                        End If
                                    Finally
                                        Runtime.InteropServices.Marshal.ReleaseComObject(entry)
                                    End Try
                                End If
                            End If
                        Finally
                            Runtime.InteropServices.Marshal.ReleaseComObject(recipient)
                        End Try
                    End If
                Next
            Finally
                Runtime.InteropServices.Marshal.ReleaseComObject(recipients)
            End Try
        End If

        Return Result.Trim()
    End Function

    Private Function ReplaceXMLSpecChar(ByVal strsource As String) As String
        Dim i As Integer
        strsource = Replace(strsource, "&", "&amp;")
        strsource = Replace(strsource, ">", "&gt;")
        strsource = Replace(strsource, "<", "&lt;")
        strsource = Replace(strsource, "'", "")
        strsource = Replace(strsource, """", "")

        For i = 128 To 254
            strsource = Replace(strsource, Chr(i), "")
        Next

        ReplaceXMLSpecChar = strsource
    End Function

    Public Function UploadHandler(ByVal MailsCollection As Collection) As Collections.Specialized.StringDictionary
        Dim OIDRefsUploadStatus As New Collections.Specialized.StringDictionary
        Dim URL As String = getProp("appurl")
        Dim responseString As String
        Dim attchNotFound As String = ""
        Dim Mail() As String

        If URL = "" Then
            MsgBox("Error: Invalid configuration data file. Please register again to fix this issue.", vbCritical, "FranConnect E-mail Uploader")
            log("UploadNow Error: Invalid configuration data file. Please register again to fix this issue.")
            Dim LoginForm As frmLogin = New frmLogin()
            LoginForm.ShowDialog()
            Return OIDRefsUploadStatus  'return blank
        Else
            URL = URL + M_SCRIPT
        End If

        Dim SuccessfulCount As Integer = 0
        Dim FailedCount As Integer = 0

        For i = 1 To MailsCollection.Count

            Mail = CType(MailsCollection.Item(i), String())
            responseString = BundleAndPostToServer(URL, Mail).Trim
            If responseString = "true" Then
                log("Upload success:" & CStr(OIDRefs.Item(i)))
                SuccessfulCount = SuccessfulCount + 1
                OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Success")
            Else
                If responseString = "" Then
                    Dim temp() As String
                    Dim filename As String

                    temp = Mail(0).Split(CChar("\"))
                    filename = temp(temp.Length - 1)
                    'to get file names
                    'split by using _
                    'the last element is file name

                    temp = filename.Split(CChar("_"))
                    filename = temp(temp.Length - 1)

                    temp = filename.Split(CChar("."))
                    filename = temp(0)
                    responseString = filename

                    OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Error")

                Else
                    OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Denied")
                End If

                log("Upload fail:" & CStr(OIDRefs.Item(i)))
                'log("Confrm fail:" & OIDRefs(responseString))

            End If
        Next
        CredentialsValidationSuccess = False    'setting value to False will enforce registration
        log("#" & SuccessfulCount & " emails out of #" & MailsCollection.Count & " have been uploaded successfuly to FranConnect.")

        If Not deleteFolder(TEMP_MAIL_FOLDER) = True Then
            log("Temprory files has failed to cleared automatically")
        Else
            log("Temprory folder has been removed successfuly")
        End If

        Return OIDRefsUploadStatus

    End Function

    Public Function UploadHandlerSU(ByVal MailsCollection As Collection) As Collections.Specialized.StringDictionary
        Dim OIDRefsUploadStatus As New Collections.Specialized.StringDictionary
        Dim URL As String = getProp("appurl")
        Dim responseString As String
        Dim attchNotFound As String = ""
        Dim Mail() As String

        If URL = "" Then
            MsgBox("Error: Invalid configuration data file. Please register again to fix this issue.", vbCritical, "FranConnect E-mail Uploader")
            log("UploadNow Error: Invalid configuration data file. Please register again to fix this issue.")
            Dim LoginForm As frmLogin = New frmLogin()
            LoginForm.ShowDialog()
            Return OIDRefsUploadStatus  'return blank
        Else
            URL = URL + M_SCRIPT
        End If

        Dim SuccessfulCount As Integer = 0
        Dim FailedCount As Integer = 0

        For i = 1 To MailsCollection.Count

            Mail = CType(MailsCollection.Item(i), String())
            responseString = BundleAndPostToServer(URL, Mail).Trim
            If responseString = "true" Then
                log("Upload success:" & CStr(OIDRefs.Item(i)))
                SuccessfulCount = SuccessfulCount + 1
                OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Success")
            Else
                If responseString = "" Then
                    Dim temp() As String
                    Dim filename As String

                    temp = Mail(0).Split(CChar("\"))
                    filename = temp(temp.Length - 1)
                    'to get file names
                    'split by using _
                    'the last element is file name

                    temp = filename.Split(CChar("_"))
                    filename = temp(temp.Length - 1)

                    temp = filename.Split(CChar("."))
                    filename = temp(0)
                    responseString = filename

                    OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Error")

                Else
                    OIDRefsUploadStatus.Add(CStr(OIDRefs.Item(i)), "Denied")
                End If

                log("Upload fail:" & CStr(OIDRefs.Item(i)))
                'log("Confrm fail:" & OIDRefs(responseString))

            End If
        Next
        CredentialsValidationSuccess = False    'setting value to False will enforce registration
        log("#" & SuccessfulCount & " emails out of #" & MailsCollection.Count & " have been uploaded successfuly to FranConnect.")

        If Not deleteFolder(TEMP_MAIL_FOLDER) = True Then
            log("Temprory files has failed to cleared automatically")
        Else
            log("Temprory folder has been removed successfuly")
        End If

        Return OIDRefsUploadStatus

    End Function


    Private Function BundleAndPostToServer(ByVal URL As String, ByVal files() As String) As String
        Try

            Dim boundary As String = "----------------------------" & DateTime.Now.Ticks.ToString("x")
            Dim boundarybytes As Byte() = System.Text.Encoding.ASCII.GetBytes(vbCr & vbLf & "--" & boundary & vbCr & vbLf)
            Dim memStream As Stream = New System.IO.MemoryStream()

            memStream.Write(boundarybytes, 0, boundarybytes.Length)

            Dim headerTemplate As String = "Content-Disposition: form-data; name=""{0}""; filename=""{1}""" & vbCr & vbLf & " Content-Type: application/octet-stream" & vbCr & vbLf & vbCr & vbLf

            Dim filename As String = ""
            Dim temp As String()
            Dim header As String
            Dim headerbytes As Byte()
            Dim fileStream As FileStream
            Dim buffer As Byte()
            Dim bytesRead As Integer

            For i As Integer = 0 To files.Length - 1
                temp = files(i).Split(CChar("\"))
                filename = temp(temp.Length - 1)
                log("uploadfile:--->" & filename)
                'logic to get file names
                'split by using _
                'the last element is file name

                'temp = filename.Split("_")
                'filename = temp(temp.Length - 1)
                header = String.Format(headerTemplate, filename, files(i))
                headerbytes = System.Text.Encoding.UTF8.GetBytes(header)
                memStream.Write(headerbytes, 0, headerbytes.Length)

                fileStream = New FileStream(files(i), FileMode.Open, FileAccess.Read)
                buffer = New Byte(1023) {}
                bytesRead = 0

                While (InlineAssignHelper(bytesRead, fileStream.Read(buffer, 0, buffer.Length))) <> 0
                    memStream.Write(buffer, 0, bytesRead)
                End While
                memStream.Write(boundarybytes, 0, boundarybytes.Length)

                fileStream.Close()
            Next

            Dim httpWebRequest2 As Net.HttpWebRequest = DirectCast(Net.HttpWebRequest.Create(URL), Net.HttpWebRequest)
            httpWebRequest2.ContentType = "multipart/form-data; boundary=" & boundary
            httpWebRequest2.Method = "POST"
            httpWebRequest2.KeepAlive = True
            httpWebRequest2.Credentials = System.Net.CredentialCache.DefaultCredentials

            httpWebRequest2.ContentLength = memStream.Length
            Dim requestStream As Stream = httpWebRequest2.GetRequestStream()
            memStream.Position = 0
            Dim tempBuffer As Byte() = New Byte(CInt(memStream.Length - 1)) {}
            memStream.Read(tempBuffer, 0, tempBuffer.Length)
            memStream.Close()
            requestStream.Write(tempBuffer, 0, tempBuffer.Length)
            requestStream.Close()

            Dim webResponse2 As Net.WebResponse = httpWebRequest2.GetResponse()
            Dim str As String = ""
            Dim reader As StreamReader = New StreamReader(webResponse2.GetResponseStream())
            Dim stream2 As Stream = webResponse2.GetResponseStream()
            Dim reader2 As New StreamReader(stream2)

            Dim WebResponse As Net.HttpWebResponse = DirectCast(httpWebRequest2.GetResponse(), Net.HttpWebResponse)
            If WebResponse.StatusCode = Net.HttpStatusCode.OK Then
                Dim readStream As New StreamReader( _
                        WebResponse.GetResponseStream(), _
                       System.Text.Encoding.GetEncoding("utf-8"))
                str = (readStream.ReadToEnd())
                log("Response---" & str)
            Else
                log("httpStatus other than OK")
            End If

            webResponse2.Close()
            httpWebRequest2 = Nothing
            webResponse2 = Nothing
            Return str

        Catch ex As System.Exception
            log("UploadToURL exception: " & ex.Message)
            Return ""
        End Try
    End Function

    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function


End Module
