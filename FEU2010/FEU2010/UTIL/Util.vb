﻿Imports System.Diagnostics
Imports System
Imports System.Net
Imports FEU2010.LoginWebService
Imports System.Net.Security
Imports System.Web.Services.Description
Imports System.ServiceModel
Imports System.IO
Imports System.Collections
Imports System.Text



Module Util
    Public Const WEBURL As String = "http://apollo.franconnect.net/acctual/actualHostName1.jsp?hostName="
    Public Const ALTWEBURL As String = "http://sai.franconnect.net/acctual/actualHostName1.jsp?hostName="

    Public gServerURL As String = "" 'this will be populated from apollo

    Public Const LOGINLINK = "Login.jws?wsdl"
    Public Const CURDIRPATH As String = "\FranConnect\EmailUploaderAddIN\"

    Public ConfigFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString & CURDIRPATH
    Public LogFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString & CURDIRPATH 'Store errorLog file path

    Public dictionaryEP As New Dictionary(Of String, String)
    Public email As String = ""
    Public arraylistpids As New ArrayList
    Public pids As String = ""



    Public Function getProp(ByVal AttributeName As String) As String
        '   This method is used for extracting the properties
        '   from the configuration file

        Dim temp1 As String
        Dim temp2 As String
        Dim indexOfEqual As Integer
        Dim indexOfString As Integer
        Dim indexOfEnd As Integer
        Dim filename As String
        Dim Result As String

        Call verifyFolderPath(ConfigFilePath)

        filename = ConfigFilePath & "config.properties"

        Result = ""
        If My.Computer.FileSystem.FileExists(filename) = False Then
            'MsgBox("Configuration data not found.")
            getProp = Result
            Exit Function
        End If

        temp1 = My.Computer.FileSystem.ReadAllText(filename, Encoding.Default)
        temp2 = ""

        indexOfString = InStr(temp1, AttributeName)

        If indexOfString > 0 Then
            indexOfEqual = InStr(indexOfString, temp1, "=", CompareMethod.Text)
            indexOfEnd = InStr(indexOfString, temp1, vbCrLf)
        End If

        If indexOfString <> 0 Then
            If AttributeName = "password" Then
                Result = DecryptText(Mid(temp1, indexOfEqual + 1, indexOfEnd - indexOfEqual - 1))

            Else
                If indexOfEnd = 0 Then
                    Result = Mid(temp1, indexOfEqual + 1, temp1.Length)
                Else
                    Result = Mid(temp1, indexOfEqual + 1, indexOfEnd - indexOfEqual - 1)
                End If
            End If
        End If
        getProp = Result
        Exit Function

errorhandler:
        LogError("getProp:Error occured. Please check the log file")
        Err.Clear()
    End Function



    Public Sub setProp(ByVal prop As String, ByVal value As String)
        '   This method is used for extracting the properties
        '   from the configuration file
        Dim temp2 As String
        Dim temp1 As String
        Dim indexOfEqual As Integer
        Dim indexOfString As Integer
        Dim indexOfEnd As Integer
        Dim filename As String

        On Error GoTo errorhandler

        If prop = "password" Then
            value = EncryptText(value)
        End If

        Call verifyFolderPath(ConfigFilePath)
        filename = ConfigFilePath & "config.properties"

        If My.Computer.FileSystem.FileExists(filename) = False Then

            temp2 = prop + "=" + value & vbCrLf
            My.Computer.FileSystem.WriteAllText(filename, temp2, False)

        Else

            temp1 = My.Computer.FileSystem.ReadAllText(filename, Encoding.Default)
            temp2 = ""
            indexOfString = InStr(temp1, prop)

            If indexOfString > 0 Then
                indexOfEqual = InStr(indexOfString, temp1, "=", CompareMethod.Text)
                indexOfEnd = InStr(indexOfString, temp1, vbCrLf)
                temp2 = Mid(temp1, 1, indexOfEqual) + value + Mid(temp1, indexOfEnd, Len(temp1))
                My.Computer.FileSystem.WriteAllText(filename, temp2, False)
            Else
                temp2 = prop + "=" + value & vbCrLf
                My.Computer.FileSystem.WriteAllText(filename, temp2, True)
            End If


        End If

        Exit Sub


errorhandler:
        MsgBox(Err.Description)
        LogError("setProp:Error occured. Please check the log file.")
        Err.Clear()
    End Sub

    Public Function EncryptText(ByVal strText As String) As String
        On Error GoTo errhandler
        Dim i As Integer, c As Integer
        Dim strBuff As String = ""
        Dim strPwd As String

        strPwd = "crypt!@#4`textpritish!vishal"
        'Encrypt string

        For i = 1 To Len(strText)
            c = Asc(Mid$(strText, i, 1))
            c = c + Asc(Mid$(strPwd, (i Mod Len(strPwd)) + 1, 1))
            strBuff = strBuff & Chr(c)
        Next i

        EncryptText = strBuff
        Exit Function
errhandler:
        LogError(Err.Source & ":" & Err.Description)
    End Function


    Public Function DecryptText(ByVal strText As String) As String
        On Error GoTo errhandler
        Dim i As Integer, c As Integer
        Dim strBuff As String = ""
        Dim strPwd As String
        strPwd = "crypt!@#4`textpritish!vishal"
        'Decrypt string
        For i = 1 To Len(strText)
            c = Asc(Mid$(strText, i, 1))
            c = c - Asc(Mid$(strPwd, (i Mod Len(strPwd)) + 1, 1))
            strBuff = strBuff & Chr(c And &HFF)
        Next i
        DecryptText = strBuff
        Exit Function
errhandler:
        LogError(Err.Source & ":" & Err.Description)
    End Function

    Public Function verifyFolderPath(ByVal folderPath As String) As Boolean
        Dim Result As Boolean
        Dim startPos As Integer
        Dim nextPos As Integer
        Dim CurFolder As String
        Dim curPath As String

        On Error GoTo errHandler

        If Not My.Computer.FileSystem.DirectoryExists(folderPath) Then
            startPos = 1
            nextPos = startPos + 1

            Do While (nextPos <> 0)
                startPos = InStr(startPos, folderPath, "\")
                nextPos = InStr(startPos + 1, folderPath, "\")
                CurFolder = Mid(folderPath, startPos + 1, (nextPos - startPos) - 1)
                curPath = Mid(folderPath, 1, startPos)
                startPos = nextPos
                'Create folder only if it does not exist at currentPath
                If Not My.Computer.FileSystem.DirectoryExists(curPath & CurFolder) Then
                    My.Computer.FileSystem.CreateDirectory(curPath & CurFolder)
                End If
                nextPos = InStr(startPos + 1, folderPath, "\")
            Loop
            Result = True
        Else
            Result = True
        End If

        verifyFolderPath = Result
        Exit Function



errHandler:
        MsgBox("Folder path verification process has encountered an error..")
        Err.Clear()
        verifyFolderPath = False

    End Function

    Public Sub log(ByVal str As String)
        '   This method is used for logging events into the log file 
        Dim filename As String

        Try
            If verifyFolderPath(LogFilePath) = True Then
                filename = LogFilePath + "EmailUploaderAddIn.log"
                str = Now & "--> " & str & vbCrLf
                My.Computer.FileSystem.WriteAllText(filename, str, True)
                Call check_logFilesize()
            End If

        Catch ex As Exception
            MsgBox("Exception while generating log. Message: " & ex.Message)
        End Try

    End Sub


    Private Sub check_logFilesize()
        '// Method to create a backup copy of log file,
        '// if the size exceeds from the specified size,
        '// limit of 1MB(1024 KB)

        Dim fileName As String
        Dim newLogfileName As String
        Dim dp(6) As String
        Dim f As IO.FileInfo
        Dim i As Integer

        On Error GoTo errHandler

        fileName = LogFilePath + "EmailUploaderAddIn.log"
        newLogfileName = LogFilePath + "LogArchive.Log"
        f = My.Computer.FileSystem.GetFileInfo(fileName)

        If My.Computer.FileSystem.FileExists(fileName) Then
            If (f.Length / 1024) > 1024 Then        'file size in KB
                'copy current log file with a new name and flush the contents of main log file
                If My.Computer.FileSystem.FileExists(newLogfileName) Then
                    My.Computer.FileSystem.DeleteFile(newLogfileName)
                End If
                My.Computer.FileSystem.CopyFile(fileName, newLogfileName, False)
                'delete the contents in a main log file]
                My.Computer.FileSystem.WriteAllText(fileName, "Log archived as: " & newLogfileName & vbCrLf, False)
            End If
        End If
        f = Nothing

        Exit Sub

errHandler:
        Err.Clear()
    End Sub

    Public Sub LogError(ByVal s As String)
        'this method is used for log the error
        Dim filename As String
        s = s + vbCrLf
        If verifyFolderPath(LogFilePath) = True Then
            filename = LogFilePath + "EmailUploaderAddIn.log"
            My.Computer.FileSystem.WriteAllText(filename, s, True)
        End If
    End Sub

    Public Sub LogDetail(ByVal a As String, ByVal b As String, ByVal e As String)
        Dim filename As String
        a = a + vbCrLf + b + vbCrLf + e + vbCrLf
        If verifyFolderPath(LogFilePath) = True Then
            filename = LogFilePath + "EmailUploaderAddIn.log"
            My.Computer.FileSystem.WriteAllText(filename, a, True)
        End If
    End Sub


    Public Function setGServerURLApollo(ByVal BuildCode As String) As String
        Dim ApolloClient As WebClient
        Dim Result As String = ""
        Dim FranURL As String = BuildCode
        Dim URLPartArray() As String

        If FranURL.Contains("/") Then
            FranURL = Microsoft.VisualBasic.Right(FranURL, Len(FranURL) - (FranURL.IndexOf("//") + 2))
            URLPartArray = FranURL.Split(CChar("/"))
            BuildCode = URLPartArray(1)
        Else
            BuildCode = FranURL
        End If


        Try
            ApolloClient = New WebClient
            gServerURL = ApolloClient.DownloadString(WEBURL & BuildCode)
            gServerURL = Trim(gServerURL.Trim)

            If Mid(gServerURL, 2, 4) = "null" Then
                log("Apollo Returned:" & gServerURL)

            ElseIf Mid(gServerURL, 2, 4) = "http" Then
                gServerURL = Mid(gServerURL, 2, InStr(2, gServerURL, "$") - 2)
                Result = gServerURL
                log("Apollo Returned:" & gServerURL)

            Else
                log("Apollo Returned:" & gServerURL)
            End If

        Catch ex As Exception
            log("Apollo Exception:" & ex.Message)
            ApolloClient = Nothing

            Try
                ApolloClient = New WebClient
                gServerURL = ApolloClient.DownloadString(ALTWEBURL & BuildCode)
                gServerURL = Trim(gServerURL.Trim)

                If Mid(gServerURL, 2, 4) = "null" Then
                    log("Apollo Returned:" & gServerURL)

                ElseIf Mid(gServerURL, 2, 4) = "http" Then
                    gServerURL = Mid(gServerURL, 2, InStr(2, gServerURL, "$") - 2)
                    Result = gServerURL
                    log("Apollo Returned:" & gServerURL)

                Else
                    log("Apollo Returned:" & gServerURL)
                End If

            Catch ex1 As Exception
                log("Sai Exception:" & ex.Message)
                ApolloClient = Nothing

            End Try
        End Try



        If Not Result = "" Then
            setProp("appurl", gServerURL)
            setProp("buildCode", BuildCode)

        End If

        setGServerURLApollo = Result

    End Function


    Public Function LoginNow(ByVal username As String, ByVal password As String, ByVal buildcode As String) As String
        Dim result As String = ""
        Dim UserNumber As String = ""
        If username = "" Or password = "" Or buildcode = "" Then
            result = "nologininputs"
        Else

            Dim ApolloResponse As String = ""

            ApolloResponse = setGServerURLApollo(buildcode)

            If Not ApolloResponse = "" Then

                UserNumber = LoginNow(username, password)

                If UserNumber = "-1" Then
                    result = "invaliduserpwd"
                    log("Error -1:validation failed! Invalid Credential")

                ElseIf UserNumber = "-9" Or UserNumber = "" Then
                    result = "webserviceexception"
                    log("Error-9: validation failed! Authentication Failed.")

                Else
                    result = UserNumber
                    log("Validataion success")
                End If
            Else
                result = "invalidbuildcode"
            End If

        End If
        setProp("UserNumber", UserNumber)
        If result = "" Then
            result = "noresponse"
        End If
        Return result
    End Function


    Private Function LoginNow(ByVal LoginID As String, ByVal Pwd As String) As String

        Dim APPURL As String = gServerURL
        Dim UserNumber As String = ""

        If Microsoft.VisualBasic.Right(APPURL, 1) = "\" Or Microsoft.VisualBasic.Right(APPURL, 1) = "/" Then
            APPURL = APPURL + LOGINLINK
        Else
            APPURL = APPURL + "/" + LOGINLINK
        End If
        APPURL = Replace(APPURL, "https", "http")
        'For TLS1.2
        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = DirectCast(3072, [SecurityProtocolType])


        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateCertificate)

        Dim LoginProxy As New LoginService

        LoginProxy.AllowAutoRedirect = True
        LoginProxy.Url = APPURL
        LoginProxy.Timeout = 10000         '10,000 ms = 10 secs

        Try
            UserNumber = LoginProxy.login(LoginID, Pwd)

        Catch ex As Exception
            UserNumber = "-9"
            log("Login method Exception: " & ex.Message)
            If Not IsNothing(ex.InnerException) Then
                log("InnerException: " & ex.InnerException.ToString)
            End If

        End Try

        If UserNumber = "-1".Trim() Then
            log("Invalid credentials! Please Enter Correct Login ID and Password.")
        ElseIf UserNumber = "" Then
            log("Authentication Failed before try.")
        ElseIf UserNumber = "-9" Then
            log("LoginAuthentication:" & UserNumber)
        Else
            log("LoginAuthentication:" & UserNumber)
        End If


        Return UserNumber

    End Function

    Public Function makeFile(ByVal data As String, ByVal fileName As String) As Boolean
        Dim Result As Boolean = False
        Try
            Dim fs As New FileStream(fileName, FileMode.Create, FileAccess.Write)
            Dim s As New StreamWriter(fs, System.Text.Encoding.UTF8)
            s.WriteLine(data)
            s.Close()
            Result = True
        Catch ex As Exception
            log("Could not make file:" & fileName & " due to exception: " & ex.Message)
        End Try
        Return Result
    End Function

    Public Function deleteFolder(ByVal exactfolderpath As String) As Boolean
        If System.IO.Directory.Exists(exactfolderpath) Then
            Try
                System.IO.Directory.Delete(exactfolderpath, True)
                Return True
            Catch ex As Exception
                log("Folder delete exception: " & ex.Message)
                Return False
            End Try
        Else
            Return False
        End If


    End Function
End Module
