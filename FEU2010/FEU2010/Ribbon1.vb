﻿Imports Microsoft.Office.Tools.Ribbon
Imports FEU2010.Util
Imports FEU2010.EmailUploaderAddIn
Imports System.Windows.Forms
Imports System.ComponentModel


Public Class Ribbon1

    Private bw As BackgroundWorker = New BackgroundWorker

    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory)
        InitializeComponent()

        Me.Tab1.Position = Me.Factory.RibbonPosition.BeforeOfficeId("TabMail")
        Me.Group1.Position = Globals.Factory.GetRibbonFactory.RibbonPosition.BeforeOfficeId("GroupMailNew")

        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
        bw.WorkerSupportsCancellation = True
        bw.WorkerReportsProgress = True
    End Sub

    Private Sub Group1_DialogLauncherClick(ByVal sender As Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Group1.DialogLauncherClick

        If System.Windows.Forms.Application.OpenForms().OfType(Of frmLogin).Any Then
            For Each loginform As frmLogin In System.Windows.Forms.Application.OpenForms
                loginform.Activate()
            Next
        Else
            Dim loginform As frmLogin = New frmLogin()
            loginform.ShowDialog(New WindowWrapper)
        End If


    End Sub

    Private Sub BtnUploadToFranConnect_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles BtnUploadToFranConnect.Click

        If bw.IsBusy Then
            If System.Windows.Forms.Application.OpenForms().OfType(Of frmWaiting).Any Then
                For Each waitform As frmWaiting In System.Windows.Forms.Application.OpenForms
                    log("Please wait activated...")
                    waitform.Activate()
                Next
            Else
                Dim WaitForm As frmWaiting = New frmWaiting()
                log("Please wait...")
                WaitForm.ShowDialog(New WindowWrapper)
            End If

        Else
            If My.Computer.FileSystem.FileExists(ConfigFilePath & "config.properties") = True Then
                'configuration file exists
                Util.pids = ""
                Group1.DialogLauncher.Enabled = False
                bw.RunWorkerAsync()
            Else
                'configuration file not found
                If System.Windows.Forms.Application.OpenForms().OfType(Of frmLogin).Any Then
                    For Each loginform As frmLogin In System.Windows.Forms.Application.OpenForms
                        loginform.Activate()
                    Next
                Else
                    Dim loginform As frmLogin = New frmLogin()
                    loginform.ShowDialog(New WindowWrapper)
                End If
            End If
        End If

    End Sub

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

        If CredentialsValidationSuccess = False Then
            Dim username As String = getProp("username")
            Dim password As String = getProp("password")
            Dim FranURL As String = getProp("buildCode")
            Dim loginResult As String

            loginResult = LoginNow(username, password, FranURL)

            Select Case loginResult

                Case "nologininputs"
                    CredentialsValidationSuccess = False
                    e.Result = loginResult

                Case "invaliduserpwd"
                    CredentialsValidationSuccess = False
                    e.Result = loginResult

                Case "webserviceexception"
                    e.Result = loginResult

                Case "invalidbuildcode"
                    e.Result = loginResult

                Case "noresponse"
                    e.Result = loginResult

                Case Else
                    'means success
                    CredentialsValidationSuccess = True
                    setProp("uploadtoken", loginResult)
                    'Execution will continue to beneath IF bloack 
            End Select

        End If

       
        If CredentialsValidationSuccess = True Then
            Dim m_SelectedMails As New Collection
            m_SelectedMails = GetOutlookItems()
            If m_SelectedMails.Count = 0 Then
                MessageBox.Show(New WindowWrapper, "Oops! No e-mail is selected to upload. ", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Cancel = True
            Else
                e.Result = UploadHandler(m_SelectedMails)

            End If
        End If



    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Dim SuccessfulCount As Integer

        Group1.DialogLauncher.Enabled = True
        CloseWaitingForm()

        If e.Cancelled = True Then
            log("Upload BW thread Cancelled")
            'MessageBox.Show(New WindowWrapper, "Upload Cancelled.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        ElseIf e.Error IsNot Nothing Then
            log("Upload BW thread Error: " & e.Error.Message)
            MessageBox.Show(New WindowWrapper, "Upload failed to complete because of thread exception.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else
            If TypeOf (e.Result) Is Collections.Specialized.StringDictionary Then
                currentUploadStatusCollection = CType(e.Result, Collections.Specialized.StringDictionary)

                Dim OrefUploadStatus As Collections.Specialized.StringDictionary
                Dim UploadStatus As String
                OrefUploadStatus = currentUploadStatusCollection

                For Each itemKey In OrefUploadStatus.Keys
                    UploadStatus = OrefUploadStatus(CStr(itemKey))
                    If UploadStatus <> "Success" Then
                        Exit For
                    Else
                        SuccessfulCount = SuccessfulCount + 1
                    End If

                Next
                If SuccessfulCount = OrefUploadStatus.Count Then
                    MessageBox.Show(New WindowWrapper, "# " & SuccessfulCount & " of total # " & OrefUploadStatus.Count & " E-mail(s) successfully uploaded.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else

                    'MessageBox.Show(New WindowWrapper, "#" & OrefUploadStatus.Count - SuccessfulCount & " emails have been uploaded to FranConnect.", "FranConnect E-mail Uploader")
                    ShowRejectedMails()
                End If

            ElseIf TypeOf (e.Result) Is String Then
                log("eResult:" & CStr(e.Result))
                Select Case CStr(e.Result)

                    Case "nologininputs"
                        MessageBox.Show(New WindowWrapper, "No/Incomplete registration! Please complete your registration.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        bw.CancelAsync()

                    Case "invaliduserpwd"
                        MessageBox.Show(New WindowWrapper, "Invalid credentials! Please verify the FranConnect Login ID and Password you have entered for registration.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        bw.CancelAsync()

                    Case "webserviceexception"
                        MessageBox.Show(New WindowWrapper, "Webservice exception! Authentication service seems to be not working at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        bw.CancelAsync()

                    Case "invalidbuildcode"
                        MessageBox.Show(New WindowWrapper, "Invalid AppURL/Buildcode! Please verify the App URL/buildcode you have entered for registration and try again.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        bw.CancelAsync()

                    Case "noresponse"
                        MessageBox.Show(New WindowWrapper, "No response! Server is not responding at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        bw.CancelAsync()

                    Case Else
                        'means success

                End Select
            End If
            'handle result here
            log("Upload BW thread completed")
        End If

    End Sub

    Private Sub ShowRejectedMails()
       
        Dim frmRejectedMails As New frmUploadResult
        If frmRejectedMails.InvokeRequired Then
            Dim d As New frmUploadResult.openUploadStatusFormCallBack(AddressOf ShowRejectedMails)
            frmRejectedMails.Invoke(d)
        Else
            frmRejectedMails.ShowDialog(New WindowWrapper)
        End If

    End Sub


    Private Sub CloseWaitingForm()
        Dim openedWaitingforms As New FormCollection
        openedWaitingforms = System.Windows.Forms.Application.OpenForms
        If System.Windows.Forms.Application.OpenForms().OfType(Of frmWaiting).Any Then
            For i = 0 To openedWaitingforms.Count - 1
                If TypeOf (System.Windows.Forms.Application.OpenForms(i)) Is frmWaiting Then
                    Dim waitform As frmWaiting = CType(System.Windows.Forms.Application.OpenForms(i), frmWaiting)
                    If waitform.InvokeRequired Then
                        Dim d As New frmWaiting.closeWaitingFormCallBack(AddressOf CloseWaitingForm)
                        waitform.Invoke(d)
                    Else
                        waitform.Close()
                    End If
                End If
            Next
        End If
    End Sub


    Private Sub btnsearchandupload_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnsearchandupload.Click

        If My.Computer.FileSystem.FileExists(ConfigFilePath & "config.properties") = True Then
            'configuration file exists

            Dim searchandupload As frmSearchandUpload = New frmSearchandUpload()

            searchandupload.ShowDialog(New WindowWrapper)
        Else
            'configuration file not found
            If System.Windows.Forms.Application.OpenForms().OfType(Of frmLogin).Any Then
                For Each loginform As frmLogin In System.Windows.Forms.Application.OpenForms
                    loginform.Activate()
                Next
            Else
                Dim loginform As frmLogin = New frmLogin()
                loginform.ShowDialog(New WindowWrapper)
            End If
        End If

        
        

    End Sub
End Class
