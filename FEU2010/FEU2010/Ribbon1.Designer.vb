﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    '' <System.Diagnostics.DebuggerNonUserCode()> _
    ''Public Sub New()
    ''    MyBase.New(Globals.Factory.GetRibbonFactory())

    ''    'This call is required by the Component Designer.
    ''    InitializeComponent()

    ''End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim RibbonDialogLauncherImpl1 As Microsoft.Office.Tools.Ribbon.RibbonDialogLauncher = Me.Factory.CreateRibbonDialogLauncher
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Ribbon1))
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.Group1 = Me.Factory.CreateRibbonGroup
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BtnUploadToFranConnect = Me.Factory.CreateRibbonButton
        Me.btnsearchandupload = Me.Factory.CreateRibbonButton
        Me.Tab1.SuspendLayout()
        Me.Group1.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.ControlId.OfficeId = "TabMail"
        Me.Tab1.Groups.Add(Me.Group1)
        Me.Tab1.Label = "TabMail"
        Me.Tab1.Name = "Tab1"
        '
        'Group1
        '
        RibbonDialogLauncherImpl1.Image = CType(resources.GetObject("RibbonDialogLauncherImpl1.Image"), System.Drawing.Image)
        RibbonDialogLauncherImpl1.ScreenTip = "Registration"
        RibbonDialogLauncherImpl1.SuperTip = "Register E-mail uploader with your FranConnect credentials."
        Me.Group1.DialogLauncher = RibbonDialogLauncherImpl1
        Me.Group1.Items.Add(Me.BtnUploadToFranConnect)
        Me.Group1.Items.Add(Me.btnsearchandupload)
        Me.Group1.Label = "Franconnect E-mail Uploader"
        Me.Group1.Name = "Group1"
        '
        'BtnUploadToFranConnect
        '
        Me.BtnUploadToFranConnect.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.BtnUploadToFranConnect.Image = Global.FEU2010.My.Resources.Resources.mailUploader32
        Me.BtnUploadToFranConnect.Label = "Upload To FranConnect"
        Me.BtnUploadToFranConnect.Name = "BtnUploadToFranConnect"
        Me.BtnUploadToFranConnect.ScreenTip = "Upload To FranConnect"
        Me.BtnUploadToFranConnect.ShowImage = True
        Me.BtnUploadToFranConnect.SuperTip = "Upload selected E-mail(s) to FranConnect."
        '
        'btnsearchandupload
        '
        Me.btnsearchandupload.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.btnsearchandupload.Image = Global.FEU2010.My.Resources.Resources.email_search_48x48
        Me.btnsearchandupload.Label = "Search and Upload"
        Me.btnsearchandupload.Name = "btnsearchandupload"
        Me.btnsearchandupload.ScreenTip = "Search and Upload"
        Me.btnsearchandupload.ShowImage = True
        Me.btnsearchandupload.SuperTip = "Search Franconnect record and choose whom to upload currently selected emails."
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Outlook.Explorer"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents Group1 As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents BtnUploadToFranConnect As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnsearchandupload As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
