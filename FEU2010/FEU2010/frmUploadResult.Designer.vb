﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUploadResult
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnOk = New System.Windows.Forms.Button()
        Me.mailListView = New System.Windows.Forms.ListView()
        Me.lblNoMatch = New System.Windows.Forms.Label()
        Me.lblErorinProcess = New System.Windows.Forms.Label()
        Me.lblCounter = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BtnOk
        '
        Me.BtnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtnOk.Location = New System.Drawing.Point(800, 313)
        Me.BtnOk.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BtnOk.Name = "BtnOk"
        Me.BtnOk.Size = New System.Drawing.Size(93, 35)
        Me.BtnOk.TabIndex = 4
        Me.BtnOk.Text = "OK"
        Me.BtnOk.UseVisualStyleBackColor = True
        '
        'mailListView
        '
        Me.mailListView.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mailListView.FullRowSelect = True
        Me.mailListView.Location = New System.Drawing.Point(0, 31)
        Me.mailListView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.mailListView.Name = "mailListView"
        Me.mailListView.Size = New System.Drawing.Size(905, 274)
        Me.mailListView.Sorting = System.Windows.Forms.SortOrder.Descending
        Me.mailListView.TabIndex = 3
        Me.mailListView.UseCompatibleStateImageBehavior = False
        '
        'lblNoMatch
        '
        Me.lblNoMatch.AutoSize = True
        Me.lblNoMatch.BackColor = System.Drawing.Color.Transparent
        Me.lblNoMatch.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoMatch.Location = New System.Drawing.Point(582, 9)
        Me.lblNoMatch.Name = "lblNoMatch"
        Me.lblNoMatch.Size = New System.Drawing.Size(139, 15)
        Me.lblNoMatch.TabIndex = 6
        Me.lblNoMatch.Text = "Reason: No match found"
        '
        'lblErorinProcess
        '
        Me.lblErorinProcess.AutoSize = True
        Me.lblErorinProcess.BackColor = System.Drawing.Color.Transparent
        Me.lblErorinProcess.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErorinProcess.Location = New System.Drawing.Point(774, 9)
        Me.lblErorinProcess.Name = "lblErorinProcess"
        Me.lblErorinProcess.Size = New System.Drawing.Size(119, 15)
        Me.lblErorinProcess.TabIndex = 7
        Me.lblErorinProcess.Text = "Reason: Process error"
        '
        'lblCounter
        '
        Me.lblCounter.AutoSize = True
        Me.lblCounter.BackColor = System.Drawing.Color.Transparent
        Me.lblCounter.Font = New System.Drawing.Font("Segoe UI", 11.25!)
        Me.lblCounter.Location = New System.Drawing.Point(12, 319)
        Me.lblCounter.Name = "lblCounter"
        Me.lblCounter.Size = New System.Drawing.Size(103, 20)
        Me.lblCounter.TabIndex = 8
        Me.lblCounter.Text = "# MailCounter"
        '
        'frmUploadResult
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackgroundImage = Global.FEU2010.My.Resources.Resources.bgBig
        Me.CancelButton = Me.BtnOk
        Me.ClientSize = New System.Drawing.Size(905, 354)
        Me.Controls.Add(Me.lblCounter)
        Me.Controls.Add(Me.lblErorinProcess)
        Me.Controls.Add(Me.lblNoMatch)
        Me.Controls.Add(Me.BtnOk)
        Me.Controls.Add(Me.mailListView)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUploadResult"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Undelivered E-mail(s)"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtnOk As System.Windows.Forms.Button
    Friend WithEvents mailListView As System.Windows.Forms.ListView
    Friend WithEvents lblNoMatch As System.Windows.Forms.Label
    Friend WithEvents lblErorinProcess As System.Windows.Forms.Label
    Friend WithEvents lblCounter As System.Windows.Forms.Label
End Class
