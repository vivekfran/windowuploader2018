﻿Imports System.IO
Imports Newtonsoft.Json.Linq
Imports System.Windows.Forms
Imports System.ComponentModel
Imports FEU2010.Util
Imports FEU2010.EmailUploaderAddIn
Imports System.Threading
Imports System.Collections
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports System.Data
Imports System.Drawing
Imports System.Net




Public Class frmSearchandUpload

    Public done As Boolean = False
    Private bw As BackgroundWorker = New BackgroundWorker
    Private bw1 As BackgroundWorker = New BackgroundWorker
    Dim searchstring As String
    Public email As String = ""
    Public pids As String = ""
    Dim dt As New DataTable
    Public row As String()


    Public Sub New()

        'MyBase.New(Globals.Factory.GetRibbonFactory)
        InitializeComponent()

        ' Me.Tab1.Position = Me.Factory.RibbonPosition.BeforeOfficeId("TabMail")
        'Me.Group1.Position = Globals.Factory.GetRibbonFactory.RibbonPosition.BeforeOfficeId("GroupMailNew")

        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
        bw.WorkerSupportsCancellation = True
        bw.WorkerReportsProgress = True

        AddHandler bw1.DoWork, AddressOf bw1_DoWork
        AddHandler bw1.RunWorkerCompleted, AddressOf bw1_RunWorkerCompleted
        bw1.WorkerSupportsCancellation = True
        bw1.WorkerReportsProgress = True

    End Sub

    Private Sub Search()
        Try






            'dgvrecord.Columns(0).Name = "Record Name"
            'dgvrecord.Columns(0).Width = 200

            'dgvrecord.Columns(1).Name = "Record Type"
            'dgvrecord.Columns(1).Width = 200
            'dgvrecord.Columns(2).Name = "Email"
            'dgvrecord.Columns(2).Width = 200
            'dgvrecord.Columns(3).Name = "Refrence"
            'dgvrecord.Columns(3).Width = 200

            'dgvrecord.Columns(4).Name = "primaryids"
            'dgvrecord.Columns(4).Width = 200
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = DirectCast(3072, [SecurityProtocolType])


            Dim test As String = gServerURL
            Dim URL As String = getProp("appurl")

            Dim uNo As String = getProp("UserNumber")



            Dim jsondataurl As String = URL + "resource/emailupload/getDetails?searchString=" + searchstring + "&userNo=" + uNo + "&fromwhere=outemailuploader"

            Dim request As Net.HttpWebRequest
            Dim response As Net.HttpWebResponse = Nothing
            Dim reader As StreamReader

            request = DirectCast(Net.WebRequest.Create(jsondataurl), Net.HttpWebRequest)

            response = DirectCast(request.GetResponse(), Net.HttpWebResponse)
            reader = New StreamReader(response.GetResponseStream())

            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            Dim root As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(rawresp)

            Dim item As JObject = JObject.Parse(rawresp)

            Dim jtoken As Newtonsoft.Json.Linq.JToken
            Dim jtokenlev2 As Newtonsoft.Json.Linq.JToken
            '  Dim items As JArray = DirectCast(item("fimInfo"), JArray)

            Dim modulename As String = ""
            Dim RecordName As String = ""
            Dim leadtabPrimaryId As String = ""
            Dim leademailid As String = ""
            Dim Refrence As String = ""
            Dim RecordType As String = ""



            Dim results As List(Of JToken) = root.Children().ToList

            For Each itemlev1 As JProperty In results
                itemlev1.CreateReader()
                Select Case itemlev1.Name


                    Case "fimInfo"

                        Dim pids As String = ""
                        Dim Recordtypevalue As String = ""
                        Dim Recordtypedisplay As String = ""


                        Dim strCC = itemlev1.Value.ToString
                        Dim rootlev2 As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(strCC)

                        Dim resultslev2 As List(Of JToken) = rootlev2.Children().ToList




                        For Each itemlev2 As JProperty In resultslev2
                            itemlev2.CreateReader()
                            Recordtypevalue = itemlev2.Name


                            jtokenlev2 = itemlev2.First

                            While jtokenlev2 IsNot Nothing

                                For Each subitem As JObject In itemlev2.Values

                                    RecordName = CStr(subitem("name"))
                                    leadtabPrimaryId = CStr(subitem("tabPrimaryId"))
                                    leademailid = CStr(subitem("emailID"))
                                    Refrence = CStr(subitem("associatedWith"))

                                    If (RecordName = Nothing) Then
                                        RecordName = ""
                                    End If

                                    If (leadtabPrimaryId = Nothing) Then
                                        leadtabPrimaryId = ""
                                    End If
                                    If (leademailid = Nothing) Then
                                        leademailid = ""
                                    End If
                                    If (Refrence = Nothing) Then
                                        Refrence = ""
                                    End If

                                    pids = "fimInfo_" + Recordtypevalue + "_" + leadtabPrimaryId

                                    Select Case Recordtypevalue.Trim

                                        Case "area"
                                            Recordtypedisplay = "Area"

                                        Case "fimInfo"
                                            Recordtypedisplay = "Info Mgr"

                                        Case "contact"
                                            Recordtypedisplay = "Contact"

                                        Case "mu"
                                            Recordtypedisplay = "Multi-Unit"

                                        Case "cm"
                                            Recordtypedisplay = "CRM"

                                        Case "fim"
                                            Recordtypedisplay = "Franchise Location"

                                        Case "broker"
                                            Recordtypedisplay = "Brokers"

                                        Case "fs"
                                            Recordtypedisplay = "Leads"

                                        Case "sales"
                                            Recordtypedisplay = "Sales"

                                        Case "entity"
                                            Recordtypedisplay = "Entity"

                                        Case "lead"
                                            Recordtypedisplay = "CRM Lead"


                                    End Select


                                    'sales_fs_123456

                                    row = New String() {RecordName, Recordtypedisplay, leademailid, Refrence, pids}

                                    dt.Rows.Add(row)



                                    ' dgvrecord.Rows.Add(row)

                                Next
                                Dim a As Integer = dt.Rows.Count
                                jtokenlev2 = jtokenlev2.[Next]
                            End While
                        Next


                    Case "sales"

                        Dim pids As String = ""
                        Dim Recordtypevalue As String = ""
                        Dim Recordtypedisplay As String = ""
                        Dim strCC = itemlev1.Value.ToString
                        Dim rootlev2 As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(strCC)

                        Dim resultslev2 As List(Of JToken) = rootlev2.Children().ToList

                        For Each itemlev2 As JProperty In resultslev2
                            itemlev2.CreateReader()
                            jtokenlev2 = itemlev2.First
                            Recordtypevalue = itemlev2.Name

                            While jtokenlev2 IsNot Nothing

                                For Each subitem As JObject In itemlev2.Values

                                    RecordName = CStr(subitem("name"))
                                    leadtabPrimaryId = CStr(subitem("tabPrimaryId"))
                                    leademailid = CStr(subitem("emailID"))


                                    Refrence = CStr(subitem("associatedWith"))

                                    If (RecordName = Nothing) Then
                                        RecordName = ""
                                    End If

                                    If (leadtabPrimaryId = Nothing) Then
                                        leadtabPrimaryId = ""
                                    End If
                                    If (leademailid = Nothing) Then
                                        leademailid = ""
                                    End If
                                    If (Refrence = Nothing) Then
                                        Refrence = ""
                                    End If

                                    pids = "sales_" + Recordtypevalue + "_" + leadtabPrimaryId


                                    Select Case Recordtypevalue.Trim

                                        Case "area"
                                            Recordtypedisplay = "Area"

                                        Case "fimInfo"
                                            Recordtypedisplay = "Info Mgr"

                                        Case "contact"
                                            Recordtypedisplay = "Contact"

                                        Case "mu"
                                            Recordtypedisplay = "Multi-Unit"

                                        Case "cm"
                                            Recordtypedisplay = "CRM"

                                        Case "fim"
                                            Recordtypedisplay = "Franchise Location"

                                        Case "broker"
                                            Recordtypedisplay = "Brokers"

                                        Case "fs"
                                            Recordtypedisplay = "Leads"

                                        Case "sales"
                                            Recordtypedisplay = "Sales"

                                        Case "entity"
                                            Recordtypedisplay = "Entity"

                                        Case "lead"
                                            Recordtypedisplay = "CRM Lead"


                                    End Select
                                    'sales_fs_123456

                                    row = New String() {RecordName, Recordtypedisplay, leademailid, Refrence, pids}

                                    dt.Rows.Add(row)
                                    'dgvrecord.Rows.Add(row)

                                Next

                                Dim b As Integer = dt.Rows.Count

                                jtokenlev2 = jtokenlev2.[Next]
                            End While
                        Next


                    Case "cm"


                        Dim pids As String = ""
                        Dim Recordtypevalue As String = ""
                        Dim Recordtypedisplay As String = ""
                        Dim strCC = itemlev1.Value.ToString
                        Dim rootlev2 As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(strCC)

                        Dim resultslev2 As List(Of JToken) = rootlev2.Children().ToList

                        For Each itemlev2 As JProperty In resultslev2
                            itemlev2.CreateReader()
                            jtokenlev2 = itemlev2.First
                            Recordtypevalue = itemlev2.Name

                            While jtokenlev2 IsNot Nothing

                                For Each subitem As JObject In itemlev2.Values

                                    RecordName = CStr(subitem("name"))
                                    leadtabPrimaryId = CStr(subitem("tabPrimaryId"))
                                    leademailid = CStr(subitem("emailID"))


                                    Refrence = CStr(subitem("associatedWith"))

                                    If (RecordName = Nothing) Then
                                        RecordName = ""
                                    End If

                                    If (leadtabPrimaryId = Nothing) Then
                                        leadtabPrimaryId = ""
                                    End If
                                    If (leademailid = Nothing) Then
                                        leademailid = ""
                                    End If
                                    If (Refrence = Nothing) Then
                                        Refrence = ""
                                    End If

                                    pids = "cm_" + Recordtypevalue + "_" + leadtabPrimaryId


                                    Select Case Recordtypevalue.Trim

                                        Case "area"
                                            Recordtypedisplay = "Area"

                                        Case "fimInfo"
                                            Recordtypedisplay = "Info Mgr"

                                        Case "contact"
                                            Recordtypedisplay = "Contact"

                                        Case "mu"
                                            Recordtypedisplay = "Multi-Unit"

                                        Case "cm"
                                            Recordtypedisplay = "CRM"

                                        Case "fim"
                                            Recordtypedisplay = "Franchise Location"

                                        Case "broker"
                                            Recordtypedisplay = "Brokers"

                                        Case "fs"
                                            Recordtypedisplay = "Leads"

                                        Case "sales"
                                            Recordtypedisplay = "Sales"

                                        Case "entity"
                                            Recordtypedisplay = "Entity"

                                        Case "lead"
                                            Recordtypedisplay = "CRM Lead"




                                    End Select
                                    'sales_fs_123456

                                    row = New String() {RecordName, Recordtypedisplay, leademailid, Refrence, pids}

                                    Try
                                        dt.Rows.Add(row)
                                    Catch ex As Exception
                                        Dim a As String = ""
                                    End Try

                                    'dgvrecord.Rows.Add(row)

                                Next

                                Dim c As Integer = dt.Rows.Count
                                jtokenlev2 = jtokenlev2.[Next]
                            End While
                        Next




                End Select
            Next

        Catch ex As Exception
            Dim aa As String = ex.Message

        End Try

        'dgvrecord.Sort(dgvrecord.Columns(0), ListSortDirection.Ascending)

    End Sub

    Private Sub frmSearchandUpload_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        txtsearch.Focus()
        btnSerach.Enabled = False
        btnUpload.Enabled = False


    End Sub

    Private Sub btnUpload_Click(sender As System.Object, e As System.EventArgs) Handles btnUpload.Click
        btnUpload.Enabled = False
        Me.Close()

        Util.pids = ""

        Dim selectedCellCount As Int32 = dgvrecord.SelectedRows.Count

        Util.arraylistpids.Clear()

        For i As Integer = 0 To selectedCellCount - 1
            Util.arraylistpids.Add(dgvrecord.SelectedRows(i).Cells(4).Value.ToString)

            Util.pids = Util.pids.Trim + Util.arraylistpids.Item(i).ToString.Trim + ","

        Next

        Dim a As Integer = Util.pids.LastIndexOf(",")
        Dim aaa As String = Util.pids.Trim.Remove(a, 1)
        Util.pids = aaa
        Dim bbb As String = ""
        upload()




    End Sub

    Private Sub upload()

        If bw.IsBusy Then
            If System.Windows.Forms.Application.OpenForms().OfType(Of frmWaiting).Any Then
                For Each waitform As frmWaiting In System.Windows.Forms.Application.OpenForms
                    log("Please wait activated...")
                    waitform.Activate()
                Next
            Else
                Dim WaitForm As frmWaiting = New frmWaiting()
                log("Please wait...")
                WaitForm.ShowDialog(New WindowWrapper)
            End If

        Else
            If My.Computer.FileSystem.FileExists(ConfigFilePath & "config.properties") = True Then
                'configuration file exists

                ' Group1.DialogLauncher.Enabled = False
                bw.RunWorkerAsync()



            Else
                'configuration file not found
                If System.Windows.Forms.Application.OpenForms().OfType(Of frmLogin).Any Then
                    For Each loginform As frmLogin In System.Windows.Forms.Application.OpenForms
                        loginform.Activate()
                    Next
                Else
                    Dim loginform As frmLogin = New frmLogin()
                    loginform.ShowDialog(New WindowWrapper)
                End If
            End If
        End If
    End Sub


    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

        If CredentialsValidationSuccess = False Then
            Dim username As String = getProp("username")
            Dim password As String = getProp("password")
            Dim FranURL As String = getProp("buildCode")
            Dim loginResult As String

            loginResult = LoginNow(username, password, FranURL)

            Select Case loginResult

                Case "nologininputs"
                    CredentialsValidationSuccess = False
                    e.Result = loginResult

                Case "invaliduserpwd"
                    CredentialsValidationSuccess = False
                    e.Result = loginResult

                Case "webserviceexception"
                    e.Result = loginResult

                Case "invalidbuildcode"
                    e.Result = loginResult

                Case "noresponse"
                    e.Result = loginResult

                Case Else
                    'means success
                    CredentialsValidationSuccess = True
                    setProp("uploadtoken", loginResult)
                    'Execution will continue to beneath IF bloack 
            End Select

        End If


        If CredentialsValidationSuccess = True Then
            Dim m_SelectedMails As New Collection

            m_SelectedMails = GetOutlookItems()
            If m_SelectedMails.Count = 0 Then
                MessageBox.Show(New WindowWrapper, "Oops! No e-mail is selected to upload. ", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Cancel = True
            Else

                e.Result = UploadHandlerSU(m_SelectedMails)

            End If
        End If



    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)

        Dim SuccessfulCount As Integer

        '  Group1.DialogLauncher.Enabled = True
        CloseWaitingForm()

        If e.Cancelled = True Then
            log("Upload BW thread Cancelled")
            'MessageBox.Show(New WindowWrapper, "Upload Cancelled.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        ElseIf e.Error IsNot Nothing Then
            log("Upload BW thread Error: " & e.Error.Message)
            MessageBox.Show(New WindowWrapper, "Upload failed to complete because of thread exception.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else
            If TypeOf (e.Result) Is Collections.Specialized.StringDictionary Then
                currentUploadStatusCollection = CType(e.Result, Collections.Specialized.StringDictionary)

                Dim OrefUploadStatus As Collections.Specialized.StringDictionary
                Dim UploadStatus As String
                OrefUploadStatus = currentUploadStatusCollection

                For Each itemKey In OrefUploadStatus.Keys

                    UploadStatus = OrefUploadStatus(CStr(itemKey))
                    If UploadStatus <> "Success" Then
                        Exit For
                    Else
                        SuccessfulCount = SuccessfulCount + 1
                    End If

                Next
                If SuccessfulCount = OrefUploadStatus.Count Then
                    MessageBox.Show(New WindowWrapper, "# " & SuccessfulCount & " of total # " & OrefUploadStatus.Count & " E-mail(s) successfully uploaded.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else

                    'MessageBox.Show(New WindowWrapper, "#" & OrefUploadStatus.Count - SuccessfulCount & " emails have been uploaded to FranConnect.", "FranConnect E-mail Uploader")
                    ShowRejectedMails()
                End If

            ElseIf TypeOf (e.Result) Is String Then
                log("eResult:" & CStr(e.Result))
                Select Case CStr(e.Result)

                    Case "nologininputs"
                        MessageBox.Show(New WindowWrapper, "No/Incomplete registration! Please complete your registration.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        bw.CancelAsync()

                    Case "invaliduserpwd"
                        MessageBox.Show(New WindowWrapper, "Invalid credentials! Please verify the FranConnect Login ID and Password you have entered for registration.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        bw.CancelAsync()

                    Case "webserviceexception"
                        MessageBox.Show(New WindowWrapper, "Webservice exception! Authentication service seems to be not working at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        bw.CancelAsync()

                    Case "invalidbuildcode"
                        MessageBox.Show(New WindowWrapper, "Invalid AppURL/Buildcode! Please verify the App URL/buildcode you have entered for registration and try again.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        bw.CancelAsync()

                    Case "noresponse"
                        MessageBox.Show(New WindowWrapper, "No response! Server is not responding at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        bw.CancelAsync()

                    Case Else
                        'means success

                End Select
            End If
            'handle result here
            log("Upload BW thread completed")
        End If

    End Sub

    Private Sub ShowRejectedMails()

        Dim frmRejectedMails As New frmUploadResult
        If frmRejectedMails.InvokeRequired Then
            Dim d As New frmUploadResult.openUploadStatusFormCallBack(AddressOf ShowRejectedMails)
            frmRejectedMails.Invoke(d)
        Else
            frmRejectedMails.ShowDialog(New WindowWrapper)
        End If

    End Sub


    Private Sub CloseWaitingForm()
        Dim openedWaitingforms As New FormCollection
        openedWaitingforms = System.Windows.Forms.Application.OpenForms
        If System.Windows.Forms.Application.OpenForms().OfType(Of frmWaiting).Any Then
            For i = 0 To openedWaitingforms.Count - 1
                If TypeOf (System.Windows.Forms.Application.OpenForms(i)) Is frmWaiting Then
                    Dim waitform As frmWaiting = CType(System.Windows.Forms.Application.OpenForms(i), frmWaiting)
                    If waitform.InvokeRequired Then
                        Dim d As New frmWaiting.closeWaitingFormCallBack(AddressOf CloseWaitingForm)
                        waitform.Invoke(d)
                    Else
                        waitform.Close()
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click

        Me.Close()

    End Sub


    Public Sub btnSerach_Click(sender As System.Object, e As System.EventArgs) Handles btnSerach.Click
        Dim searchStringValue As String = ""
        searchStringValue = txtsearch.Text.Trim()
        If searchStringValue.Length < 3 Then
            MsgBox("Please enter any valid text to search.")
        Else
            dgvrecord.DataSource = Nothing
            dt.Rows.Clear()
            'dgvrecord.Rows.Clear()

            If dgvrecord.Rows.Count > 0 Then
                'dt.Columns.Clear()
                'dt.Rows.Clear()
                Try

                    For i As Integer = 0 To dgvrecord.Rows.Count - 1
                        dgvrecord.Rows.RemoveAt(i)
                    Next

                    'dgvrecord = New DataGridView

                    'dgvrecord.Rows.Clear()
                Catch ex As Exception

                End Try

            Else
                'dgvrecord.Columns(4).Visible = False
            End If


            If dt.Columns.Count <= 0 Then
                dt.Columns.Add("Record", GetType(String))
                dt.Columns.Add("Record Type", GetType(String))
                dt.Columns.Add("Email", GetType(String))
                dt.Columns.Add("Refrence", GetType(String))
                dt.Columns.Add("primaryids", GetType(String))


            End If




            lblRecordCount.Text = "Searching..."
            'dgvrecord.Rows.Clear()
            searchstring = txtsearch.Text.Trim



            'dgvrecord.ColumnCount = 5
            'dgvrecord.Columns(0).Name = "Record Name"
            'dgvrecord.Columns(0).Width = 200

            'dgvrecord.Columns(1).Name = "Record Type"
            'dgvrecord.Columns(1).Width = 200
            'dgvrecord.Columns(2).Name = "Email"
            'dgvrecord.Columns(2).Width = 200
            'dgvrecord.Columns(3).Name = "Refrence"
            'dgvrecord.Columns(3).Width = 200

            'dgvrecord.Columns(4).Name = "primaryids"
            'dgvrecord.Columns(4).Width = 200

            If bw1.IsBusy = False Then
                bw1.RunWorkerAsync()

            End If



            'For i = 0 To dgvrecord.Columns.Count - 1

            '    dgvrecord.Columns.Item(i).SortMode = DataGridViewColumnSortMode.NotSortable
            '    dgvrecord.Columns(i).Width = 700

            '    'dgvrecord.AutoResizeColumn()



            'Next i

        End If


    End Sub


    Private Sub bw1_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        Search()

    End Sub
    Private Sub bw1_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)



        'dgvrecord.data

        lblRecordCount.Text = "" + "records found."
        Dim count As Integer = dt.Rows.Count
        If count > 0 Then
            lblRecordCount.Text = count.ToString() + " " + lblRecordCount.Text
            Dim myfont As New Drawing.Font("Sans Serif", 12, FontStyle.Regular)
            lblRecordCount.Font = myfont

            Dim dgcolumncount As Integer = dgvrecord.Columns.Count

            dgvrecord.DataSource = dt

            dgvrecord.SelectionMode = DataGridViewSelectionMode.FullRowSelect

            ' dgvrecord.ScrollBars = ScrollBars.Vertical



            Dim a As Integer = dt.Rows.Count

            For i = 0 To dgvrecord.Columns.Count - 1

                dgvrecord.Columns.Item(i).SortMode = DataGridViewColumnSortMode.NotSortable
                dgvrecord.Columns(i).Width = 194

                'dgvrecord.AutoResizeColumn()

                'dgvrecord.Rows(1).Selected = True

            Next i

            'dgvrecord.Columns(1).MinimumWidth = 80
            'dgvrecord.AutoSizeColumnsMode = _
            'DataGridViewAutoSizeColumnsMode.AllCells

            ' dgvrecord.Rows[0].Selected = true

            dgvrecord.Sort(dgvrecord.Columns(0), ListSortDirection.Ascending)
            dgvrecord.Columns(4).Visible = False
            dgvrecord.FirstDisplayedScrollingRowIndex = 0
        Else
            lblRecordCount.Text = "No " + lblRecordCount.Text
        End If
        'CellBorderStyle = DataGridViewCellBorderStyle.None;  
        'dgvrecord.CellBorderStyle = DataGridViewCellBorderStyle.None
        dgvrecord.GridColor = Color.WhiteSmoke







        'dgvrecord.Rows.RemoveAt(dgvrecord.Rows.Count - 1)



    End Sub

    Private Sub txtsearch_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtsearch.KeyPress

        Dim searchStringValuebtn As String = ""
        searchStringValuebtn = txtsearch.Text.Trim()

        'If searchStringValue.Length > 3 Then

        '    btnSerach.Enabled = True
        'Else
        '    btnSerach.Enabled = False
        'End If

        'If searchStringValue.Length < 3 Then
        '    btnSerach.Enabled = False
        'End If

        If searchStringValuebtn.Length < 2 Then
            btnSerach.Enabled = False
        Else
            btnSerach.Enabled = True
        End If

        If e.KeyChar = Chr(13) Then 'Chr(13) is the Enter Key
            'Runs the Button1_Click Event

          
            Dim searchStringValue1 As String = ""
            searchStringValue1 = txtsearch.Text.Trim()
            If searchStringValue1.Length < 3 Then



                MsgBox("Please enter any valid text to search.")

            Else
                btnSerach_Click(Me, EventArgs.Empty)
            End If

        End If







    End Sub


    'Private Sub dgvrecord_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvrecord.CellContentClick

    '    btnUpload.Enabled = True
    'End Sub

    Private Sub dgvrecord_RowEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvrecord.RowEnter
        btnUpload.Enabled = True
    End Sub
End Class






