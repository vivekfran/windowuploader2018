﻿
Imports System.Windows.Forms
Public Class frmUploadResult

    Dim OutlookNameSpaceid As Microsoft.Office.Interop.Outlook.NameSpace
    Dim OutlookApp As Microsoft.Office.Interop.Outlook.Application
    Dim OlMailItem As Microsoft.Office.Interop.Outlook.MailItem

    Delegate Sub openUploadStatusFormCallBack()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
 
    End Sub
    Private Sub lvMails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        mailListView.Clear()
        mailListView.Items.Clear()
        mailListView.View = Windows.Forms.View.Details
        mailListView.AllowColumnReorder = True

        mailListView.Columns.Add("Date", 120, Windows.Forms.HorizontalAlignment.Center).TextAlign = HorizontalAlignment.Left
        mailListView.Columns.Add("Subject", 300, Windows.Forms.HorizontalAlignment.Center).TextAlign = HorizontalAlignment.Left
        mailListView.Columns.Add("From", 120, Windows.Forms.HorizontalAlignment.Center).TextAlign = HorizontalAlignment.Left
        mailListView.Columns.Add("To", 180, Windows.Forms.HorizontalAlignment.Center).TextAlign = HorizontalAlignment.Left
        mailListView.Columns.Add("CC", 180, Windows.Forms.HorizontalAlignment.Center).TextAlign = HorizontalAlignment.Left
        'mailListView.BackColor = Drawing.Color.WhiteSmoke

        lblNoMatch.ForeColor = Drawing.Color.Black
        lblErorinProcess.ForeColor = Drawing.Color.DarkRed
        searchMail(currentUploadStatusCollection)
    End Sub

    Public Sub searchMail(ByVal OrefUploadStatus As Collections.Specialized.StringDictionary)
        Try
            Dim FailCount As Integer = 0
            Dim ShowSubject As String = ""
            Dim ShowFrom As String = ""
            Dim ShowTO As String = ""
            Dim ShowCC As String = ""
            Dim ShowDate As String = ""
            Dim UploadStatus As String = ""
            Dim listItem As ListViewItem


            OutlookApp = New Microsoft.Office.Interop.Outlook.Application
            OutlookNameSpaceid = OutlookApp.GetNamespace("MAPI")


            For Each itemKey In OrefUploadStatus.Keys
                UploadStatus = OrefUploadStatus(CStr(itemKey))
                If UploadStatus <> "Success" Then
                    FailCount = FailCount + 1
                    OlMailItem = CType((OutlookNameSpaceid.GetItemFromID(CStr(itemKey))), Outlook.MailItem)
                    'Get Subject
                    If IsNothing(OlMailItem.Subject) Then
                        ShowSubject = ""
                    Else
                        ShowSubject = OlMailItem.Subject
                    End If

                    'Get mail CC
                    If IsNothing(OlMailItem.CC) Then
                        ShowCC = ""
                    Else
                        ShowCC = (OlMailItem.CC.ToString())
                    End If

                    'Get mail to
                    ShowTO = OlMailItem.To

                    'Get mail From
                    ShowFrom = OlMailItem.SenderName

                    'Get Date
                    ShowDate = CStr(OlMailItem.CreationTime)

                    listItem = mailListView.Items.Add(ShowDate)
                    listItem.SubItems.Add(ShowSubject)
                    listItem.SubItems.Add(ShowFrom)
                    listItem.SubItems.Add(ShowTO)
                    listItem.SubItems.Add(ShowCC)
                    'listItem.BackColor = Drawing.Color.Ivory
                    Select Case UploadStatus
                        Case "Success"
                            listItem.ForeColor = Drawing.Color.DarkGreen
                        Case "Denied"
                            listItem.ForeColor = Drawing.Color.Black
                        Case "Error"
                            listItem.ForeColor = Drawing.Color.DarkRed
                        Case Else
                            listItem.ForeColor = Drawing.Color.DarkGray
                    End Select
                End If

            Next
            lblCounter.Text = "# " & FailCount & " of total # " & OrefUploadStatus.Count & " E-mails failed to upload at the moment."

            OutlookNameSpaceid = Nothing
            OutlookApp = Nothing
            OlMailItem = Nothing
        Catch ex As System.Exception
            lblCounter.Text = "Warning: Exception while retrieving upload status."
            log("mailstatus search exception:" & ex.Message)

        End Try

    End Sub


    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Me.Close()
    End Sub


End Class