﻿Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Windows.Forms


Public Class frmLogin
    Private bw As BackgroundWorker = New BackgroundWorker
    Dim username As String
    Dim password As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted

        bw.WorkerSupportsCancellation = True
    End Sub

    Private Sub frmLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bw.IsBusy Then e.Cancel = True
    End Sub



    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        TxtName.Text = getProp("username")
        TxtPassword.Text = getProp("password")
        TxtBuildCode.Text = getProp("buildCode")
    End Sub


    Private Sub BtnRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRegister.Click

        If bw.IsBusy Then
            'do nothing because previuos request is still pending
        Else
            BtnRegister.Enabled = False
            ProgressBar1.Style = ProgressBarStyle.Marquee
            'CredentialsValidationSuccess = False    'setting value to False will enforce registration
            bw.RunWorkerAsync()

        End If


    End Sub


    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        bw.CancelAsync()
    End Sub


    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)


        username = (TxtName.Text).Trim
        password = (TxtPassword.Text).Trim
        Dim FranURL As String = (TxtBuildCode.Text).Trim


        e.Result = LoginNow(username, password, FranURL)



        If bw.CancellationPending = True Then
            e.Cancel = True
        End If

    End Sub


    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        BtnRegister.Enabled = True
        ProgressBar1.Style = ProgressBarStyle.Continuous

        If e.Cancelled = True Then
            MessageBox.Show(Me, "Registration request has Cancelled.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            log("BW Cancelled Login")

        ElseIf e.Error IsNot Nothing Then
            MessageBox.Show(Me, "Thread error")
            log("BW Error Login: " & e.Error.Message)
        Else


            'handle result here
            log("BW Done")
            Dim result As String = e.Result.ToString

            Select Case result

                Case "nologininputs"
                    MessageBox.Show(Me, "No/Incomplete inputs! Please enter your FranConnect credentials and application URL.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    bw.CancelAsync()

                Case "invaliduserpwd"
                    MessageBox.Show(Me, "Invalid credentials! Please verify the FranConnect Login ID and Password you have entered.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    bw.CancelAsync()

                Case "webserviceexception"
                    MessageBox.Show(Me, "Webservice exception! Authentication service seems to be not working at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    bw.CancelAsync()

                Case "invalidbuildcode"
                    MessageBox.Show(Me, "Invalid AppURL/Buildcode! Please verify the App URL/buildcode you have entered and try again.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    bw.CancelAsync()

                Case "noresponse"
                    MessageBox.Show(Me, "No response! Server is not responding at the moment. Please try later.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    bw.CancelAsync()

                Case Else
                    'means success
                    setProp("username", username)
                    setProp("password", password)

                    'CredentialsValidationSuccess = False
                    MessageBox.Show(Me, "Registration successfull! To upload, simply select the e-mail(s), and click the 'Upload To FranConnect' button.", "FranConnect E-mail Uploader", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Close()
            End Select


        End If

    End Sub

    Private Sub PicHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PicHelp.Click
        ToolTip1.SetToolTip(Me.PicHelp, "Please open the FranConnect Application in browser" & vbCrLf & "copy the URL (web address) and paste here.")

    End Sub

    Private Sub PicHelp_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PicHelp.MouseHover
        ToolTip1.SetToolTip(Me.PicHelp, "Please open the FranConnect Application in browser" & vbCrLf & "copy the URL (web address) and paste here.")

    End Sub
End Class
